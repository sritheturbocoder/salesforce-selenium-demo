package com.salesforce.selenium.demo.selenium_poc;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class MainPageTest {
    private WebDriver driver;


    @BeforeMethod
    public void setUp() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://login.salesforce.com");
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void loginToSalesforce() throws InterruptedException {
        driver.findElement(By.xpath(".//*[@id='username']")).sendKeys(System.getProperty("arg1"));
        driver.findElement(By.xpath(".//*[@id='password']")).sendKeys(System.getProperty("arg2"));
        driver.findElement(By.xpath(".//*[@id='Login']")).click();
        Thread.sleep(5000);
        assertTrue(driver.getTitle().contains("Home | Salesforce"));
    }

    @Test
    public void globalSearchTest() throws InterruptedException {
        loginToSalesforce();
        WebElement inputBox = driver.findElement(By.xpath("//input[@title='Search...']"));
        inputBox.click();
        inputBox.clear();
        inputBox.sendKeys("University of Arizona");
        inputBox.click();
        driver.findElement(By.xpath("//span[@title='University of Arizona']"));
        inputBox.sendKeys(Keys.RETURN);
        driver.findElement(By.xpath("//a[@title='University of Arizona']"));
    }
}
